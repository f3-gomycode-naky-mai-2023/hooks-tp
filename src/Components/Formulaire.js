import React, { useEffect } from 'react';
import { useState } from 'react';
import Button from 'react-bootstrap/Button';
import Col from 'react-bootstrap/Col';
import Form from 'react-bootstrap/Form';
import InputGroup from 'react-bootstrap/InputGroup';
import Row from 'react-bootstrap/Row';
import Table from 'react-bootstrap/Table';

const Formulaire = () => {
             const [validated, setValidated] = useState(false);
             const [nom, setNom] = useState("");
             const [prenom, setPrenom] = useState("");
             const [pseudo, setPseudo] = useState("");
             const [email, setEmail] = useState("");
             const [personnes,setPersonnes] = useState([]);

             useEffect(()=>{
                setPersonnes([
                    {nom:"kouakou", prenom:"jean", pseudo:"rickson", email:"jeankouakou@gmail.com"},
                    {nom:"bakari", prenom:"samake", pseudo:"ablo", email:"samake45@gmail.com"},
                ])
             },[])
             useEffect(()=>{
                console.log("bonjour !!");
             },[personnes])


             const handleSubmit = (event) => {
                event.preventDefault();

             const form = event.currentTarget;
             if (form.checkValidity() === false) {
             
             event.stopPropagation();
             }
              setPersonnes([...personnes, {nom, prenom, pseudo, email}])
              setValidated(true);
            };

     return (
        <>
            <Form noValidate validated={validated} onSubmit={handleSubmit}>
                <Row className="mb-3">
                <Form.Group as={Col} md="4" controlId="validationCustom01">
                
                <Form.Control
                required
                type="text"
                placeholder="veuillez entrée votre nom svp"
                defaultValue={nom} 
                onChange ={(e)=>setNom(e.target.value)}
                />
                <Form.Control.Feedback>Looks good!</Form.Control.Feedback>
            </Form.Group>
            <Form.Group as={Col} md="4" controlId="validationCustom02">
                
                <Form.Control
                required
                type="text"
                placeholder="veuillez entrée votre prenom svp"
                defaultValue={prenom}
                onChange={(e)=>setPrenom(e.target.value)}
                />
                <Form.Control.Feedback>Looks good!</Form.Control.Feedback>
            </Form.Group>
            <Form.Group as={Col} md="4" controlId="validationCustomUsername">
                
                <InputGroup hasValidation>
                <InputGroup.Text id="inputGroupPrepend">@</InputGroup.Text>
                <Form.Control
                    type="text"
                    placeholder="veuillez entrée votre nom d'utilisateur svp"
                    aria-describedby="inputGroupPrepend"
                    required
                    defaultValue={pseudo}
                    onChange={(e)=>setPseudo(e.target.value)}
                />
                <Form.Control.Feedback type="invalid">
                    choisir un pseudo valide svp.
                </Form.Control.Feedback>
                </InputGroup>
            </Form.Group>
            </Row>
            <Row className="mb-3">
             <Form.Group as={Col} md="6" controlId="validationCustom03">
            
                <Form.Control type="text" placeholder="veuillez entrée votre email svp" required 
                defaultValue={email} onChange={(e)=>setEmail(e.target.value)} />
                <Form.Control.Feedback type="invalid">
                veuillez entrée un email valide svp.
                </Form.Control.Feedback>
             </Form.Group>
            
            </Row>
        
            <Button type="submit">envoyer</Button>
            {/*<Button type="button" onClick={(e)=>handleDelete(current.id)}>supprimer</Button>*/}
        </Form>
        <Table striped>
      <thead>
        <tr>
          <th>#</th>
          <th>nom</th>
          <th>prenom</th>
          <th>pseudo</th>
          <th>email</th>
        </tr>
      </thead>
      <tbody>
        {personnes  && personnes.map((person, key)=> (
        <tr key={key}>
          <td>{key}</td>
          <td>{person.nom}</td>
          <td>{person.prenom}</td>
          <td>{person.pseudo}</td>
          <td>{person.email}</td>
        </tr>
      ))}
       
      </tbody>
    </Table>
        </>
        
            );
}

export default Formulaire;
